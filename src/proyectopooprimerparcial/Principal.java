/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopooprimerparcial;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import com.csvreader.CsvWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jeanca Moreano
 */
public class Principal {

    static Calendar calendario = new GregorianCalendar();
    public static ArrayList<Ciudadano> tranquilos = new ArrayList<>();
    public static ArrayList<Agente> policias = new ArrayList<>();
    public static ArrayList<Ciudadano> ciudadanosRebeldes = new ArrayList<>();
    public static ArrayList<Ciudadano> ciudadanos = new ArrayList<>();
    static int rebeldes;
    static int tran;
    static int contador_arrestados = 0;
    static Scanner sc = new Scanner(System.in);
    static Random r = new Random();
    static Random r1 = new Random();
    static Agente[][] agentes;
    static String black = "\033[30m";
    static int vision;
    static int filas;
    static int columnas;
    static int t = 1;
    static int constant;
    static int turnos;
    static double p_ciu;
    static double p_po;
    static double legitimidad;
    static String opcion;

    public static void main(String[] args) {

        int i = 2;
        System.out.println("Ingrese numero de días: ");
        turnos = sc.nextInt();
        System.out.println("Ingrese numero filas: ");
        filas = sc.nextInt();
        if (filas>=51){
            do{
                 System.out.println("Vuelva a ingresar numero de filas (no sobrepasar de 50): ");
                 filas=sc.nextInt();
  
             }while(filas>=51);
        }
        System.out.println("Ingrese numero columnas: ");
        columnas = sc.nextInt();
        if (columnas>=51){
            do{
                 System.out.println("Vuelva a ingresar numero de columnas (no sobrepasar de 50): ");
                 columnas=sc.nextInt();
  
             }while(columnas>=51);
        }
        System.out.println("Ingrese Porcentaje ciudadanos: ");
        p_ciu = sc.nextDouble();
        System.out.println("Ingrese Porcentaje policias: ");
        p_po = sc.nextDouble();
        System.out.println("Ingrese visión de policias y ciudadanos: ");
        vision = sc.nextInt();
        sc.nextLine();
        System.out.println("Ingrese movimiento poblacion (on o off):");
        opcion = sc.nextLine();
        System.out.println("Ingrese legitimidad de gobierno valores entre (0 a 1): ");
        legitimidad = sc.nextDouble();
        String legitimidad2=String.valueOf(legitimidad);
        if (legitimidad2.contains(",")){
                        legitimidad2=legitimidad2.replace(',', '.');
                        legitimidad=Double.parseDouble(legitimidad2);
                    }else {
                        legitimidad=Double.parseDouble(legitimidad2);
                    }
        
        Universo.EstablecerLegitimidad(legitimidad);
        System.out.println("Turno: " + (i - 1));
        situarAgentes(filas, columnas, p_ciu, p_po);
        ContarTranquilos();
        rebeldes = ciudadanosRebeldes.size();
        CrearArchivo2();
        imprimirMatriz();
        CrearArchivo();
        if (opcion.equalsIgnoreCase("on")) {

            while (i <= turnos) {
                Actualizar();
                ContarTranquilos();
                t++;
                System.out.println("Turno" + i);
                MovimientoC();
                constant = 0;
                MovimientoP();
                ActualizarPolicia();
                rebeldes -= constant;
                imprimirMatriz();

                CrearArchivo();
                i++;

            }
        } else if (opcion.equalsIgnoreCase("off")) {

            while (i <= turnos) {
                Actualizar();
                ContarTranquilos();
                t++;
                System.out.println("Turno" + i);
                constant = 0;
                MovimientoP();
                ActualizarPolicia();
                rebeldes -= constant;
                imprimirMatriz();
                CrearArchivo();
                i++;

            }
        }

    }
    
    public static void situarAgentes(int x, int y, double ciu, double poli) {

        int cantidad_cuadros = x * y;
        agentes = new Agente[x][y];
        int cantidad_ciudadanos = (int) Math.round((cantidad_cuadros * ciu) / 100);
        int cantidad_policias = (int) Math.round((cantidad_cuadros * poli) / 100);

        //crear ciudadanos
        for (int i = 1; i <= cantidad_ciudadanos; i++) {
            int nr = r.nextInt(x);
            int nr2 = r.nextInt(y);
            if (agentes[nr][nr2] == null) {
                agentes[nr][nr2] = new Ciudadano(r.nextDouble(), r.nextDouble(), nr, nr2);
                Ciudadano c = (Ciudadano) agentes[nr][nr2];
                ciudadanos.add(c);
                if (c.getEstadoCiudadano().equalsIgnoreCase("Activo")) {
                    ciudadanosRebeldes.add(c);
                } else if (c.getEstadoCiudadano().equalsIgnoreCase("Inactivo")) {
                    tranquilos.add(c);
                }

            } else {
                while (true) {
                    nr = r.nextInt(x);
                    nr2 = r.nextInt(y);
                    if (agentes[nr][nr2] == null) {
                        agentes[nr][nr2] = new Ciudadano(r.nextDouble(), r.nextDouble(), nr, nr2);
                        Ciudadano c = (Ciudadano) agentes[nr][nr2];
                        ciudadanos.add(c);
                        if (c.getEstadoCiudadano().equalsIgnoreCase("Activo")) {
                            ciudadanosRebeldes.add(c);
                        } else if (c.getEstadoCiudadano().equalsIgnoreCase("Inactivo")) {
                            tranquilos.add(c);
                        }
                        break;
                    }
                }
            }
        }//crear policias

        for (int i = 1; i <= cantidad_policias; i++) {
            int nr = r.nextInt(x);
            int nr2 = r.nextInt(y);
            if (agentes[nr][nr2] == null) {
                agentes[nr][nr2] = new Policia(nr, nr2);
                policias.add(agentes[nr][nr2]);

            } else {
                while (true) {
                    nr = r.nextInt(x);
                    nr2 = r.nextInt(y);
                    if (agentes[nr][nr2] == null) {
                        agentes[nr][nr2] = new Policia(nr, nr2);
                        policias.add(agentes[nr][nr2]);
                        break;
                    }
                }
            }

        }//llenar espacios en blanco de agentes
        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {
                if (agentes[i][j] == null) {
                    agentes[i][j] = new Agente(i, j);

                }
            }

        }

        /*for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {
                if (agentes[i][j] instanceof Ciudadano) {
                    System.out.println("hola");
                }
            }
        }*/
    }

    public static void MovimientoC() {

        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {

                if (agentes[i][j] instanceof Ciudadano) {
                    int rx = r.nextInt(vision + 1);
                    int ry = r1.nextInt(vision + 1);
                    int coordenadax = agentes[i][j].getPosicion_x();
                    int coordenaday = agentes[i][j].getPosicion_y();
                    int acumulador = 0;

                    if ((coordenadax - rx) > 0 && (coordenaday - ry) > 0) {
                        if (agentes[coordenadax - rx][coordenaday - ry].getClass().getSimpleName().equals("Agente")) {
                            agentes[coordenadax - rx][coordenaday - ry] = agentes[i][j];
                            agentes[i][j] = new Agente(i, j);
                        }

                    } else if ((coordenadax - rx) > 0 && (coordenaday + ry) < columnas) {
                        if (agentes[coordenadax - rx][coordenaday + ry].getClass().getSimpleName().equals("Agente")) {
                            agentes[coordenadax - rx][coordenaday + ry] = agentes[i][j];
                            agentes[i][j] = new Agente(i, j);
                        }
                    } else if ((coordenadax + rx) < filas && (coordenaday - ry) > 0) {
                        if (agentes[coordenadax + rx][coordenaday - ry].getClass().getSimpleName().equals("Agente")) {
                            agentes[coordenadax + rx][coordenaday - ry] = agentes[i][j];
                            agentes[i][j] = new Agente(i, j);
                        }
                    } else if ((coordenadax + rx) < filas && (coordenaday + ry) < columnas) {
                        if (agentes[coordenadax + rx][coordenaday + ry].getClass().getSimpleName().equals("Agente")) {
                            agentes[coordenadax + rx][coordenaday + ry] = agentes[i][j];
                            agentes[i][j] = new Agente(i, j);
                        }
                    }

                }
            }
        }
    }

    public static void TransformarEspaciosBlanco() {
        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {
                if (agentes[i][j].getClass().getSimpleName().equals("Agente")) {
                    agentes[i][j] = null;
                }

            }
        }
    }

    public static void imprimirMatriz() {
        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {

                System.out.print(" " + agentes[i][j]);
                System.out.print(black + "");

            }
            System.out.println();
        }
    }

    public static void MovimientoP() {

        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {

                if (agentes[i][j] instanceof Policia) {

                    while (true) {

                        int rx = r.nextInt(vision + 1);
                        int ry = r1.nextInt(vision + 1);
                        int coordenadax = agentes[i][j].getPosicion_x();
                        int coordenaday = agentes[i][j].getPosicion_y();

                        if ((coordenadax - rx) > 0 && (coordenaday - ry) > 0) {

                            if (agentes[coordenadax - rx][coordenaday - ry].getClass().getSimpleName().equals("Ciudadano") && ((Ciudadano) agentes[coordenadax - rx][coordenaday - ry]).getEstadoCiudadano().equals("Activo") && ((Policia) agentes[i][j]).isRevelado() == false) {
                                agentes[coordenadax - rx][coordenaday - ry] = agentes[i][j];
                                agentes[i][j] = new Agente(i, j);
                                contador_arrestados++;
                                constant++;

                                break;
                            }

                        } else if ((coordenadax - rx) > 0 && (coordenaday + ry) < columnas) {
                            if (agentes[coordenadax - rx][coordenaday + ry].getClass().getSimpleName().equals("Ciudadano") && ((Ciudadano) agentes[coordenadax - rx][coordenaday + ry]).getEstadoCiudadano().equals("Activo") && ((Policia) agentes[i][j]).isRevelado() == false) {
                                agentes[coordenadax - rx][coordenaday + ry] = agentes[i][j];
                                agentes[i][j] = new Agente(i, j);
                                contador_arrestados++;
                                constant++;
                                break;
                            }
                        } else if ((coordenadax + rx) < filas && (coordenaday - ry) > 0) {
                            if (agentes[coordenadax + rx][coordenaday - ry].getClass().getSimpleName().equals("Ciudadano") && ((Ciudadano) agentes[coordenadax + rx][coordenaday - ry]).getEstadoCiudadano().equals("Activo") && ((Policia) agentes[i][j]).isRevelado() == false) {
                                agentes[coordenadax + rx][coordenaday - ry] = agentes[i][j];
                                agentes[i][j] = new Agente(i, j);
                                contador_arrestados++;
                                constant++;
                                break;
                            }
                        } else if ((coordenadax + rx) < filas && (coordenaday + ry) < columnas) {
                            if (agentes[coordenadax + rx][coordenaday + ry].getClass().getSimpleName().equals("Ciudadano") && ((Ciudadano) agentes[coordenadax + rx][coordenaday + ry]).getEstadoCiudadano().equals("Activo") && ((Policia) agentes[i][j]).isRevelado() == false) {
                                agentes[coordenadax + rx][coordenaday + ry] = agentes[i][j];
                                agentes[i][j] = new Agente(i, j);
                                contador_arrestados++;
                                constant++;
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    public static void CrearArchivo() {
        Date date = new Date();
        DateFormat hourdate = new SimpleDateFormat("HH.mm.ss dd-MM-yyyy");
        String Historial = hourdate.format(date);
        String nombreArchivo = "Simulacion";
        try {
            CsvWriter salidaCSV = new CsvWriter(new FileWriter(nombreArchivo + "_" + Historial + ".csv", true), ',');
            salidaCSV.write("Numero de turno");
            salidaCSV.write("# de agentes Tranquilos");
            salidaCSV.write("# de agentes Rebelandose");
            salidaCSV.write("#numero agentes en la carcel");
            salidaCSV.endRecord();

            salidaCSV.write(String.valueOf(t));
            salidaCSV.write(String.valueOf(tran));
            salidaCSV.write(String.valueOf(rebeldes));
            salidaCSV.write(String.valueOf(contador_arrestados));
            salidaCSV.endRecord();

            salidaCSV.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void Actualizar() {
        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {

                if (agentes[i][j] instanceof Ciudadano) {
                    ((Ciudadano) agentes[i][j]).ActualizarEstado();
                }
            }
        }
    }

    public static void ActualizarPolicia() {
        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {

                if (agentes[i][j] instanceof Policia) {
                    ((Policia) agentes[i][j]).ActualizarP();
                }
            }
        }
    }

    public static void ContarTranquilos() {
        
        for (int i = 0; i < agentes.length; i++) {
            for (int j = 0; j < agentes[i].length; j++) {

                if (agentes[i][j] instanceof Ciudadano && ((Ciudadano) agentes[i][j]).getEstadoCiudadano().equals("Inactivo")) {
                    tran++;
                }
            }
        }
        
    }

    public static void CrearArchivo2() {
        Date date = new Date();
        DateFormat hourdate = new SimpleDateFormat("HH.mm.ss dd-MM-yyyy");
        String Historial = hourdate.format(date);
        String nombreArchivo = "Parametros";
        try {
            CsvWriter salidaCSV = new CsvWriter(new FileWriter(nombreArchivo + "_" + Historial + ".csv", true), ',');
            salidaCSV.write("Numero de turno");
            salidaCSV.write("Numero de filas");
            salidaCSV.write("Numero de columnas");
            salidaCSV.write("Porcentaje de ciudadanos");
            salidaCSV.write("Porcentaje de Policias");
            salidaCSV.write("Cuadros de visio");
            salidaCSV.write("Movimiento de la poblacion");
            salidaCSV.write("Legitimidad de Gobierno");
            salidaCSV.endRecord();

            salidaCSV.write(String.valueOf(turnos));
            salidaCSV.write(String.valueOf(filas));
            salidaCSV.write(String.valueOf(columnas));
            salidaCSV.write(String.valueOf(columnas));
            salidaCSV.write(String.valueOf(p_ciu + "%"));
            salidaCSV.write(String.valueOf(p_po + "%"));
            salidaCSV.write(String.valueOf(vision));
            salidaCSV.write(opcion);
            salidaCSV.write(String.valueOf(legitimidad));
            salidaCSV.endRecord();

            salidaCSV.close();
        } catch (Exception e) {
        }
    }

}
