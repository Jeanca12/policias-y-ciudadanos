/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopooprimerparcial;

import java.util.Random;

/**
 *
 * @author Jeanca Moreano
 */
public  class Agente {

    Random r = new Random();
    String reset="\u001B[0m";String green="\033[32m"; 
    private int posicion_x;
    private int posicion_y;

    public Agente(int posicion_x, int posicion_y) {
        this.posicion_x = posicion_x;
        this.posicion_y = posicion_y;
    }

    public int CampoVision() {
        return 1;
    }

    @Override
    public String toString() {
        return "◙";
    }

    public int getPosicion_x() {
        return posicion_x;
    }

    public int getPosicion_y() {
        return posicion_y;
    }

}
