/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopooprimerparcial;

/**
 *
 * @author Jeanca Moreano
 */
public class Ciudadano extends Agente {

    String red = "\033[31m";
    String green = "\033[32m";
    private double nivelAgravio;
    private double PerjucioPercibido;
    private double ProbablidadDetencion;
    private double AversionRiesgo;
    private double RiesgoNeto;
    private boolean EstadoRebeldia;
    private String EstadoCiudadano;
    private int TiempoPrision;

    public Ciudadano(double PerjucioPercibido, double AversionRiesgo, int posicion_x, int posicion_y) {
        super(posicion_x, posicion_y);

        this.PerjucioPercibido = PerjucioPercibido;
        this.AversionRiesgo = AversionRiesgo;
        nivelAgravio = PerjucioPercibido * (1 - Universo.Legitimidad_Gobierno);
        RiesgoNeto = AversionRiesgo * ProbablidadDetencion;
        EstadoRebeldia = limiteRevelarse();
        EstadoCiudadano = AsignarEstado();

    }

    public double getNivelAgravio() {
        return nivelAgravio;
    }

    @Override
    public String toString() {
        if (limiteRevelarse()) {
            return red + "◙";
        }
        return green + "◙";
    }


    public boolean limiteRevelarse() {
        if ((nivelAgravio - RiesgoNeto) > Universo.limite) {
            return true;
        }
        return false;
    }

    public String AsignarEstado() {
        if (limiteRevelarse() == true) {
            return "Activo";
        }
        return "Inactivo";
    }

    public double getRiesgoNeto() {
        return RiesgoNeto;
    }
    
    public void probabilidadDetencion(){
        this.ProbablidadDetencion = 1 - Math.exp((-2.3) * Math.round(Principal.policias.size()/Principal.rebeldes));
    }

    public String getEstadoCiudadano() {
        return EstadoCiudadano;
    }
    
    public void ActualizarEstado(){
       probabilidadDetencion();
       ActualizarRiesgoNeto();
       limiteRevelarse();
       AsignarEstado();
    }
    public void ActualizarRiesgoNeto(){
        this.RiesgoNeto = AversionRiesgo * ProbablidadDetencion;
    }
    



}
