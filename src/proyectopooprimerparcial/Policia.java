/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopooprimerparcial;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Jeanca Moreano
 */
public class Policia extends Agente {
String purple="\033[35m"; 

    Random r = new Random();
    String yellow="\033[33m"; 
    String blue = "\033[34m";
    private double valentia;
    private boolean revelado;
    public static ArrayList<Ciudadano> ciudadano;

    public Policia(int posicion_x, int posicion_y) {
        super(posicion_x, posicion_y);
        
    }

    @Override
    public String toString() {
        if (LimiteValentia())
            return yellow+"◙"; 

        return blue + "◙";
    }

    public void DeterminarValentia() {
        double v = r.nextDouble();
        valentia = v;
    }

    public boolean LimiteValentia() {
        DeterminarValentia();
        if (valentia > 0.65) {
            revelado = true;
            return true;
        }
        revelado = false;
        return false;
    }

    public boolean isRevelado() {
        return revelado;
    }
    
    public void ActualizarP(){
       
        LimiteValentia();
    }
    
}
